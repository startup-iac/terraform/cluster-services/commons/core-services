# For more info view:
# https://artifacthub.io/packages/helm/nginx/nginx-ingress

# Alternative Chart 
# https://kubernetes.github.io/ingress-nginx/deploy/

locals {
  prefix_file = lower(var.cluster_type)
  values_file = "${local.prefix_file}-values-${var.chart_version}.yaml"
}

data "template_file" "values" {
  template = "${file("${path.module}/${local.values_file}")}"
  #vars = {
  #  consul_address = "${aws_instance.consul.private_ip}"
  #}
}

resource "helm_release" "this" {
  name             = "main"
  namespace        = var.namespace
  version          = var.chart_version

  repository = "https://helm.nginx.com/stable"
  chart      = "nginx-ingress"

  values = [
    data.template_file.values.rendered
  ]

  set {
    name  = "controller.service.loadBalancerIP"
    value = var.ingress_ip
  } 


}


#nginx.ingress.kubernetes.io/secure-backends: "true"
#nginx.ingress.kubernetes.io/protocol: "https"
#nginx.ingress.kubernetes.io/ssl-redirect: "true"
#nginx.ingress.kubernetes.io/proxy-cookie-path: "/ /; HTTPOnly; Secure"
