variable "namespace" {
  type    = string
}

variable "chart_version" {
  type    = string
}