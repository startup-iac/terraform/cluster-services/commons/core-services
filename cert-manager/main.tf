# For more info view:
# https://artifacthub.io/packages/helm/cert-manager/cert-manager
# Extra Info
# https://cert-manager.io/docs/installation/helm/

resource "helm_release" "this" {
  name             = "cert-manager"
  namespace        = var.namespace
  version          = var.chart_version

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"

  set {
    name  = "installCRDs"
    value = true
  }

  set {
    name  = "global.leaderElection.namespace" #GKE Autopilot fix
    value = var.namespace
  }

}

